/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 * @author Cprados
 */
public class Aplicacion implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext contenedor) throws ServletException {
        WebApplicationContext contextoSpring = getContext();
        ServletRegistration.Dynamic servlet = contenedor.addServlet("despachador", new DispatcherServlet(contextoSpring));
        servlet.setLoadOnStartup(2);
        servlet.addMapping("/");
    }

    private WebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext contexto = new AnnotationConfigWebApplicationContext();
        String nombreClase = this.getClass().getCanonicalName();
        String paqueteClase = nombreClase.substring(0, nombreClase.lastIndexOf('.'));
        contexto.setConfigLocation(paqueteClase);
        return contexto;
    }
}