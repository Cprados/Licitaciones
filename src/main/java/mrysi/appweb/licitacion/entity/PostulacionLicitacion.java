/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

/**
 *
 * @author Cprados
 */
public class PostulacionLicitacion {
    
    private int idPostulacion;
    private Licitacion licitacionPostulacion;
    private Proveedor proveedorPostulado;
    private Consorcio consorcio;

    public PostulacionLicitacion(int idPostulacion, Licitacion licitacionPostulacion, Proveedor proveedorPostulado, Consorcio consorcio) {
        this.idPostulacion = idPostulacion;
        this.licitacionPostulacion = licitacionPostulacion;
        this.proveedorPostulado = proveedorPostulado;
        this.consorcio = consorcio;
    }

    public Consorcio getConsorcio() {
        return consorcio;
    }

    public void setConsorcio(Consorcio consorcio) {
        this.consorcio = consorcio;
    }

    

    public int getIdPostulacion() {
        return idPostulacion;
    }

    public void setIdPostulacion(int idPostulacion) {
        this.idPostulacion = idPostulacion;
    }

    public Licitacion getLicitacionPostulacion() {
        return licitacionPostulacion;
    }

    public void setLicitacionPostulacion(Licitacion licitacionPostulacion) {
        this.licitacionPostulacion = licitacionPostulacion;
    }

    public Proveedor getProveedorPostulado() {
        return proveedorPostulado;
    }

    public void setProveedorPostulado(Proveedor proveedorPostulado) {
        this.proveedorPostulado = proveedorPostulado;
    }
    
    
}
