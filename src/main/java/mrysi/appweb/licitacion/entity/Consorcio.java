/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cprados
 */
public class Consorcio {
    
    private int idConsorcio;
    private PostulacionLicitacion postulacion;
    private String nombre;
    private Date fechaCreacion;
    private List<InvitacionConsorcio> ltInvitados;

    public Consorcio(int idConsorcio, PostulacionLicitacion postulacion, String nombre, Date fechaCreacion, List<InvitacionConsorcio> ltInvitados) {
        this.idConsorcio = idConsorcio;
        this.postulacion = postulacion;
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.ltInvitados = ltInvitados;
    }

    public List<InvitacionConsorcio> getLtInvitados() {
        return ltInvitados;
    }

    public void setLtInvitados(List<InvitacionConsorcio> ltInvitados) {
        this.ltInvitados = ltInvitados;
    }

    

    public int getIdConsorcio() {
        return idConsorcio;
    }

    public void setIdConsorcio(int idConsorcio) {
        this.idConsorcio = idConsorcio;
    }

    public PostulacionLicitacion getPostulacion() {
        return postulacion;
    }

    public void setPostulacion(PostulacionLicitacion postulacion) {
        this.postulacion = postulacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    
}
