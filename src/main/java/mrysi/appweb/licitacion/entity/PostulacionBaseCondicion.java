/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

/**
 *
 * @author Cprados
 */
public class PostulacionBaseCondicion {
    
    private int idPostulacion;
    private String comentario;
    private PostulacionLicitacion postulacion;
    private BasesCondicion condicion;

    public PostulacionBaseCondicion(int idPostulacion, String comentario, PostulacionLicitacion postulacion, BasesCondicion condicion) {
        this.idPostulacion = idPostulacion;
        this.comentario = comentario;
        this.postulacion = postulacion;
        this.condicion = condicion;
    }

    public int getIdPostulacion() {
        return idPostulacion;
    }

    public void setIdPostulacion(int idPostulacion) {
        this.idPostulacion = idPostulacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public PostulacionLicitacion getPostulacion() {
        return postulacion;
    }

    public void setPostulacion(PostulacionLicitacion postulacion) {
        this.postulacion = postulacion;
    }

    public BasesCondicion getCondicion() {
        return condicion;
    }

    public void setCondicion(BasesCondicion condicion) {
        this.condicion = condicion;
    }
    
    
}
