/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cprados
 */
public class Proveedor {
    
    private String rfc;
    private String NombreRazonSocial;
    private String telefono;
    private String nombreRepresentante;
    private char razonSocial;
    private Date fechaRegistro;
    private List<PostulacionLicitacion> ltPostulaciones;
    private List<InvitacionConsorcio> ltInvitaciones;

    public Proveedor(String rfc, String NombreRazonSocial, String telefono, String nombreRepresentante, char razonSocial, Date fechaRegistro, List<PostulacionLicitacion> ltPostulaciones) {
        this.rfc = rfc;
        this.NombreRazonSocial = NombreRazonSocial;
        this.telefono = telefono;
        this.nombreRepresentante = nombreRepresentante;
        this.razonSocial = razonSocial;
        this.fechaRegistro = fechaRegistro;
        this.ltPostulaciones = ltPostulaciones;
    }
    
    public List<PostulacionLicitacion> getLtPostulaciones() {
        return ltPostulaciones;
    }

    public void setLtPostulaciones(List<PostulacionLicitacion> ltPostulaciones) {
        this.ltPostulaciones = ltPostulaciones;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreRazonSocial() {
        return NombreRazonSocial;
    }

    public void setNombreRazonSocial(String NombreRazonSocial) {
        this.NombreRazonSocial = NombreRazonSocial;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreRepresentante() {
        return nombreRepresentante;
    }

    public void setNombreRepresentante(String nombreRepresentante) {
        this.nombreRepresentante = nombreRepresentante;
    }

    public char getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(char razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    
    
}
