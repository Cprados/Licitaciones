/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

/**
 *
 * @author Cprados
 */
public class InvitacionConsorcio {
    
    private int idInvitacionConsorcio;
    private String mensaje;
    private boolean confirmado;
    private String respuestaMsj;
    private Consorcio consorcio;
    private Proveedor integrante;

    public InvitacionConsorcio(int idInvitacionConsorcio, String mensaje, boolean confirmado, String respuestaMsj, Consorcio consorcio, Proveedor integrante) {
        this.idInvitacionConsorcio = idInvitacionConsorcio;
        this.mensaje = mensaje;
        this.confirmado = confirmado;
        this.respuestaMsj = respuestaMsj;
        this.consorcio = consorcio;
        this.integrante = integrante;
    }

    public Proveedor getIntegrante() {
        return integrante;
    }

    public void setIntegrante(Proveedor integrante) {
        this.integrante = integrante;
    }

    

    public int getIdInvitacionConsorcio() {
        return idInvitacionConsorcio;
    }

    public void setIdInvitacionConsorcio(int idInvitacionConsorcio) {
        this.idInvitacionConsorcio = idInvitacionConsorcio;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isConfirmado() {
        return confirmado;
    }

    public void setConfirmado(boolean confirmado) {
        this.confirmado = confirmado;
    }

    public String getRespuestaMsj() {
        return respuestaMsj;
    }

    public void setRespuestaMsj(String respuestaMsj) {
        this.respuestaMsj = respuestaMsj;
    }

    public Consorcio getConsorcio() {
        return consorcio;
    }

    public void setConsorcio(Consorcio consorcio) {
        this.consorcio = consorcio;
    }
    
    
}
