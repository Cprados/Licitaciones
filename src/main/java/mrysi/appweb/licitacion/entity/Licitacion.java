/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cprados
 */
public class Licitacion {
    private int idLicitacion;
    private String nombre;
    private Date fechaInicio;
    private Date fechaCierre;
    private Contratista contratista;
    private List<Pregunta> ltPreguntas;
    private List<PostulacionLicitacion> ltPostulaciones;
    private List<BasesCondicion> ltBasesCondiciones;

    public Licitacion(int idLicitacion, String nombre, Date fechaInicio, Date fechaCierre, Contratista contratista, List<Pregunta> ltPreguntas, List<PostulacionLicitacion> ltPostulaciones, List<BasesCondicion> ltBasesCondiciones) {
        this.idLicitacion = idLicitacion;
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaCierre = fechaCierre;
        this.contratista = contratista;
        this.ltPreguntas = ltPreguntas;
        this.ltPostulaciones = ltPostulaciones;
        this.ltBasesCondiciones = ltBasesCondiciones;
    }

    public List<BasesCondicion> getLtBasesCondiciones() {
        return ltBasesCondiciones;
    }

    public void setLtBasesCondiciones(List<BasesCondicion> ltBasesCondiciones) {
        this.ltBasesCondiciones = ltBasesCondiciones;
    }

    public List<PostulacionLicitacion> getLtPostulaciones() {
        return ltPostulaciones;
    }

    public void setLtPostulaciones(List<PostulacionLicitacion> ltPostulaciones) {
        this.ltPostulaciones = ltPostulaciones;
    }
    
    public List<Pregunta> getLtPreguntas() {
        return ltPreguntas;
    }

    public void setLtPreguntas(List<Pregunta> ltPreguntas) {
        this.ltPreguntas = ltPreguntas;
    }

    public int getIdLicitacion() {
        return idLicitacion;
    }

    public void setIdLicitacion(int idLicitacion) {
        this.idLicitacion = idLicitacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public Contratista getContratista() {
        return contratista;
    }

    public void setContratista(Contratista contratista) {
        this.contratista = contratista;
    }

    
}
