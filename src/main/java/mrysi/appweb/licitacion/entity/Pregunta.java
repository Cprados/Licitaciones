/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

import java.util.Date;

/**
 *
 * @author Cprados
 */
public class Pregunta {
    private int idPregunta;
    private String pregunta;
    private String respuesta;
    private Date fechaPregunta;
    private Date fechaRespuesta;
    private Licitacion licitacionPregunta;
    private Proveedor proveedorPregunta;

    public Pregunta(int idPregunta, String pregunta, String respuesta, Date fechaPregunta, Date fechaRespuesta, Licitacion licitacionPregunta, Proveedor proveedorPregunta) {
        this.idPregunta = idPregunta;
        this.pregunta = pregunta;
        this.respuesta = respuesta;
        this.fechaPregunta = fechaPregunta;
        this.fechaRespuesta = fechaRespuesta;
        this.licitacionPregunta = licitacionPregunta;
        this.proveedorPregunta = proveedorPregunta;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Date getFechaPregunta() {
        return fechaPregunta;
    }

    public void setFechaPregunta(Date fechaPregunta) {
        this.fechaPregunta = fechaPregunta;
    }

    public Date getFechaRespuesta() {
        return fechaRespuesta;
    }

    public void setFechaRespuesta(Date fechaRespuesta) {
        this.fechaRespuesta = fechaRespuesta;
    }

    public Licitacion getLicitacionPregunta() {
        return licitacionPregunta;
    }

    public void setLicitacionPregunta(Licitacion licitacionPregunta) {
        this.licitacionPregunta = licitacionPregunta;
    }

    public Proveedor getProveedorPregunta() {
        return proveedorPregunta;
    }

    public void setProveedorPregunta(Proveedor proveedorPregunta) {
        this.proveedorPregunta = proveedorPregunta;
    }
    
    
}
