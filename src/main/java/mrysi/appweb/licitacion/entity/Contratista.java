/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Cprados
 */
public class Contratista {
    private int idContratista;
    private String nombre;
    private String direccion;
    private String telefono;
    private List<Licitacion> ltLicitaciones;

    public Contratista(int idContratista, String nombre, String direccion, String telefono, List<Licitacion> ltLicitaciones) {
        this.idContratista = idContratista;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ltLicitaciones = ltLicitaciones;
    }

    public int getIdContratista() {
        return idContratista;
    }

    public void setIdContratista(int idContratista) {
        this.idContratista = idContratista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String Nombre) {
        this.nombre = Nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String Direccion) {
        this.direccion = Direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String Telefono) {
        this.telefono = Telefono;
    }

    public List<Licitacion> getLtLicitaciones() {
        return ltLicitaciones;
    }

    public void setLtLicitaciones(List<Licitacion> ltLicitaciones) {
        this.ltLicitaciones = ltLicitaciones;
    }
  
}
