/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.entity;

/**
 *
 * @author Cprados
 */
public class BasesCondicion {
    
    private int idCondicion;
    private String condicion;
    private Licitacion licitacionCondicion;

    public BasesCondicion(int idCondicion, String condicion, Licitacion licitacionCondicion) {
        this.idCondicion = idCondicion;
        this.condicion = condicion;
        this.licitacionCondicion = licitacionCondicion;
    }

    public int getIdCondicion() {
        return idCondicion;
    }

    public void setIdCondicion(int idCondicion) {
        this.idCondicion = idCondicion;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public Licitacion getLicitacionCondicion() {
        return licitacionCondicion;
    }

    public void setLicitacionCondicion(Licitacion licitacionCondicion) {
        this.licitacionCondicion = licitacionCondicion;
    }
    
    
}
