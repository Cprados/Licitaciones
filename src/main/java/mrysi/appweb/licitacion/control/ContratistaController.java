/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.control;

import mrysi.appweb.licitacion.entity.Contratista;
import mrysi.appweb.licitacion.oad.OadContratista;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cprados
 */
@RestController
@RequestMapping("/contratista")
public class ContratistaController {
    
    @Autowired
    OadContratista oadContratista;
    
    @GetMapping("/{idContratista}")
    public Contratista consultarContratista(int idContratista){
        return oadContratista.findByidContratista(idContratista);
    }
}
