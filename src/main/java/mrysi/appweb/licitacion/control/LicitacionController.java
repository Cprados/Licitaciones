/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.control;

import java.util.List;
import mrysi.appweb.licitacion.oad.OadLicitaciones;
import mrysi.appweb.licitacion.entity.Licitacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cprados
 */

@RestController
@RequestMapping("/licitacionesRest")
public class LicitacionController {
    
    @Autowired
    OadLicitaciones oadLicitacion;
    
    @GetMapping("")
    public List<Licitacion> listarLicitaciones(){
        return oadLicitacion.findAll();
    }
    
    @GetMapping("/{idLicitacion}")
    public Licitacion consultarLicitacion(int idLicitacion){
        return null;
    }
}
