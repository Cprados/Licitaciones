/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.oad;

import java.io.Serializable;
import mrysi.appweb.licitacion.entity.Contratista;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Cprados
 */
public interface OadContratista extends JpaRepository<Contratista, String> {
    
    Contratista findByidContratista(int idContratista);
}
