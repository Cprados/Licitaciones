/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.oad;

import java.util.List;
import mrysi.appweb.licitacion.entity.Pregunta;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Cprados
 */
public interface OadPregunta extends JpaRepository<Pregunta, String> {
    
    List<Pregunta> findByidPregunta(int idPregunta);
    
}
