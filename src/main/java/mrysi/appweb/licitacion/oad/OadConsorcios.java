/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.oad;

import java.util.List;
import mrysi.appweb.licitacion.entity.Consorcio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Cprados
 */
public interface OadConsorcios {
    
    Consorcio findByConsorcio(int idCosorcio);
}
