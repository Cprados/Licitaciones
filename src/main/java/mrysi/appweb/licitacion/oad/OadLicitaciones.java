/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.appweb.licitacion.oad;

import java.util.List;
import mrysi.appweb.licitacion.entity.Licitacion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Cprados
 */
public interface OadLicitaciones extends JpaRepository<Licitacion, String> {
    //idLicitacion
    Licitacion findByidLicitacion(int idLicitacion);
    
}
